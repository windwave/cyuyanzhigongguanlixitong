# C语言职工管理系统

#### 介绍
学校要求实训内容弄个C语言职工管理系统，感觉还可以用，那就上传我的第一个开源项目吧！


#### 使用说明

1.关于输入的有：creat_linkList2()，continueInput(linkList s)，
    creat_linkList2（）：登录系统后，若要录入职工信息，并且与程序绑定的文件为空，则调用此函数，以录入工号、姓名、部门、工作时间、年龄、性别。
    continueInput(linkList s)：与上面的函数相似，如果文件中已有信息，则调用此函数，创建新信息。
2.修改的功能有：changeLinked(linkList p) 
    changeLinked(linkList p)，给每一个查询到的职工信息绑定此函数，显示是否要修改它，通过函数所提示的，则能顺利完成修改或删除，

3.查找并显示功能有：displayList()，print(linkList p); printAll(linkList p); batch(linkList head)
    displayList（）：展示所有已录入的职工信息。
    print(linkList p);和printAll（linkList p）是显示功能的细化，让界面更加有层次性，更美观。
4.菜单栏：menu()。
    通过menu（），来进入主界面。
5.登录：
	Inspect（）：进入系统后，首先要登录，登录成功，则是高级管理员，否则是普通管理员。


